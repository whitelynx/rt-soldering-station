#ifndef IRON_HEATER_H
#define	IRON_HEATER_H

long _ironHeaterNextOn = 0;
long _ironHeaterNextOff = 10000;
unsigned char _ironHeaterPulseWidth = 0;


inline void ironHeater_setPulseWidth(unsigned char pulseWidth) {
	_ironHeaterPulseWidth = pulseWidth;
	analogWrite(PIN_HEATER_CONTROL, _ironHeaterPulseWidth);
}

inline void ironHeater_pause() {
	analogWrite(PIN_HEATER_CONTROL, 0);
}

inline void ironHeater_resume() {
	analogWrite(PIN_HEATER_CONTROL, _ironHeaterPulseWidth);
}

inline void ironHeater_update() {
	/*
	long now = millis();
	if (now >= _ironHeaterNextOn) {
		_ironHeaterNextOff = _ironHeaterNextOn + _ironHeaterPulseWidth * HEATER_PULSE_PERIOD_100MS;
		_ironHeaterNextOn += 100 * HEATER_PULSE_PERIOD_100MS;
		digitalWrite(PIN_HEATER_CONTROL, HIGH);
	} else if (now >= _ironHeaterNextOff) {
		digitalWrite(PIN_HEATER_CONTROL, LOW);
	}
	*/
}

#endif	/* IRON_HEATER_H */
