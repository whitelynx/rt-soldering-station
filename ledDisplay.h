#ifndef LED_DISPLAY_H
#define	LED_DISPLAY_H

///// LED display characters
/* LED display segments:
 *    A
 *   ___
 * F|   |B
 *  |___|
 * E| G |C
 *  |___|.DP
 *    D
 */
const unsigned char ledDigits[] = {
	//PGFEDCBA
	0b00111111, // 0
	0b00000110, // 1
	0b01011011, // 2
	0b01001111, // 3
	0b01100110, // 4
	0b01101101, // 5
	0b01111101, // 6
	0b00000111, // 7
	0b01111111, // 8
	0b01100111, // 9
};
const unsigned char ledChase1[] = {
	//PGFEDCBA
	0b00000001,
	0b00000011,
	0b00000010,
	0b00000110,
	0b00000100,
	0b00001100,
	0b00001000,
	0b00011000,
	0b00010000,
	0b00110000,
	0b00100000,
	0b00100001,
};
const unsigned char ledChase2[] = {
	//PGFEDCBA
	0b00000001,
	0b00000010,
	0b01000000,
	0b00010000,
	0b00001000,
	0b00000100,
	0b01000000,
	0b00100000,
};
const unsigned char ledC = 0b00111001;
const unsigned char ledF = 0b01110001;
const unsigned char ledSpace = 0b00000000;

unsigned char _ledCharacters[4] = { 0, 0, 0, 0 };
unsigned char _ledLastCharacterIndex = 0;
long _ledNextUpdate = 0;


void bitsToPins(int data, int startPin, int length)
{
	for (int index = 0; index < length; index++) {
		digitalWrite(startPin + index, bitRead(data, index));
	}
}

inline void led_writeCharacter(unsigned char character) {
	bitsToPins(~character, PIN_LED_DISPLAY_CATHODE_1, 8);
}

/**
 * @param position the position of the character (0 = rightmost, 3 = leftmost)
 */
inline void led_setCharacter(unsigned char position, int character) {
	_ledCharacters[position] = character;
}

inline void led_setDigit(unsigned char position, int digit) {
	led_setCharacter(position, ledDigits[digit]);
}

inline void led_setChase1(unsigned char position, int frame) {
	led_setCharacter(position, ledChase1[frame % 12]);
}

inline void led_setChase2(unsigned char position, int frame) {
	led_setCharacter(position, ledChase2[frame % 8]);
}

inline void led_clear() {
	for (unsigned char position = 0; position < 4; position++) {
		led_setCharacter(position, 0);
	}
}

inline void led_setNumber(int input) {
	int rest = input;
	for (unsigned char index = 0; index < 4; index++) {
		if (index != 0 && rest == 0) {
			led_setCharacter(index, 0);
		} else {
			led_setDigit(index, rest % 10);
		}
		rest /= 10;
	}
}

inline void led_update() {
	long now = millis();
	if (now >= _ledNextUpdate) {
		_ledNextUpdate += LED_DISPLAY_UPDATE_PERIOD_MS;
		_ledLastCharacterIndex = (_ledLastCharacterIndex + 1) % 4;
		led_writeCharacter(_ledCharacters[_ledLastCharacterIndex]);
		bitsToPins(~(1 << _ledLastCharacterIndex), PIN_LED_DISPLAY_ANODE_1, 4);
		//bitsToPins(0b1111, A0, 4);
	}
}

#endif	/* LED_DISPLAY_H */
