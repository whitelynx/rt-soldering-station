rt-soldering-station
====================

[![Hackaday.io](https://img.shields.io/badge/Hackaday-1A1A1A?logo=hackaday&logoColor=fff&style=for-the-badge "Visit us on Hackaday.io!")](https://hackaday.io/project/161071-rt-soldering-station)

A simple soldering station for Weller RT tips, using parts I had on hand. (based around an Arduino Nano clone)

![Photo of the original prototype](https://cdn.hackaday.io/images/9949251569397001401.jpg "Photo of the original prototype")


Getting Started
---------------

You can build and upload in the Arduino IDE, but this also includes a `Makefile` to make your life easier if you prefer the command line.

### Building

```sh
make
```

### Uploading

Using the USB bootloader:
```sh
make upload
```

Using the USBtinyISP: (assumes it appears as `/dev/ttyACM0`)
```sh
make upload-usbtiny
```

Using an FTDI or other USB-to-serial device: (assumes it appears as `/dev/ttyUSB0`; override by passing `FTDI_DEV=/dev/your-buspirate-device`)
```sh
make upload-ftdi
```

Using the Bus Pirate: (assumes you have its device symlinked to `/dev/buspirate`; override by passing `BUS_PIRATE_DEV=/dev/your-buspirate-device`)
```sh
make upload-buspirate
```
