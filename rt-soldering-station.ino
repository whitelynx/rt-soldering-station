#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include "ad8495.h"


//#define NO_HEATER 1 // Disable heater!

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

/// The period of the heater waveform, measured in 100ms intervals
#define HEATER_PULSE_PERIOD_100MS 2

#define INITIAL_TEMPERATURE 20
#define MAX_TEMPERATURE 400
#define MIN_TEMPERATURE 10
#define TEMPERATURE_INCREMENT 5
#define MAX_HEATER_PULSE_WIDTH 180

#define SENSOR_CHECK_INTERVAL 500
#define TEMP_INCREMENT_INTERVAL 100
#define DELAY_BEFORE_READING_TEMP_MS 50
#define DELAY_BETWEEN_TEMP_READINGS_MS 5

///// Pins
/* Arduino Nano
 * Connections:
 * D2-D3 (I2C) => OLED
 * D4-D5 <= D-pad up/down
 * D6 <= D-pad center
 * D9 => heater control
 * A0 <= thermocouple amp
 */
#define PIN_DOWN_BUTTON 4
#define PIN_UP_BUTTON 5
#define PIN_MODE_BUTTON 6
#define PIN_HEATER_CONTROL 9
#define PIN_TEMP_SENSOR A0

#define SERIAL_BAUD_RATE 9600


#ifndef NO_HEATER
#	include "ironHeater.h"
#endif // !NO_HEATER

int targetTemperature = INITIAL_TEMPERATURE;

long nextSensorCheck = 0;
long nextTempIncrement = 0;


void setup()
{
	Serial.begin(SERIAL_BAUD_RATE);

	pinMode(PIN_DOWN_BUTTON, INPUT_PULLUP);
	pinMode(PIN_UP_BUTTON, INPUT_PULLUP);
	pinMode(PIN_MODE_BUTTON, INPUT_PULLUP);
#ifndef NO_HEATER
	pinMode(PIN_HEATER_CONTROL, OUTPUT);
#endif // !NO_HEATER

	pinMode(PIN_TEMP_SENSOR, INPUT);

	// Wait for serial port to connect. Needed for native USB
	//while (!Serial) {}

	// SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
	if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
		Serial.println(F("SSD1306 allocation failed"));
		for(;;); // Don't proceed, loop forever
	}

	// Set text display options
	display.setTextSize(1); // Draw 2X-scale text
	display.setTextColor(WHITE);
	display.cp437(true);

	// Show initial display buffer contents on the screen --
	// the library initializes this with an Adafruit splash screen.
	display.display();
	delay(500);

	// Set up TIMER1B (pin 10) for heater control so we're not switching as often.
	// This helps prevent heating up the MOSFET.
	//TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM11) | _BV(WGM10);
	TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM11);
	//TCCR1B = _BV(CS12) | _BV(CS10);
	TCCR1B = _BV(CS12);
}

int currentTemperature;
boolean heating = false;

void loop()
{
	// Read target temperature input
	char delta = 0;
	if (digitalRead(PIN_DOWN_BUTTON)) {
		delta -= TEMPERATURE_INCREMENT;
	}
	if (digitalRead(PIN_UP_BUTTON)) {
		delta += TEMPERATURE_INCREMENT;
	}

	long now = millis();

	if (delta != 0) {
		if (now >= nextTempIncrement) {
			nextTempIncrement = now + TEMP_INCREMENT_INTERVAL;
			targetTemperature += delta;
			if (targetTemperature > MAX_TEMPERATURE) {
				targetTemperature = MAX_TEMPERATURE;
			} else if (targetTemperature < MIN_TEMPERATURE) {
				targetTemperature = MIN_TEMPERATURE;
			}
			Serial.print(F("Target temp: "));
			Serial.println(targetTemperature, DEC);
		}
	} else {
		nextTempIncrement = now + TEMP_INCREMENT_INTERVAL;
	}

	// Read temperature and toggle heater
	if (now >= nextSensorCheck) {
		nextSensorCheck += SENSOR_CHECK_INTERVAL;

		ironHeater_pause();
		delay(DELAY_BEFORE_READING_TEMP_MS);
		currentTemperature = 0;
		for (char i = 0; i < 5; i++) {
			currentTemperature += ad8495_getTemperature(ad8495_getVoltage(analogRead(PIN_TEMP_SENSOR)));
			delay(DELAY_BETWEEN_TEMP_READINGS_MS);
		}
		currentTemperature /= 5;
		ironHeater_resume();

		Serial.print(F("Current temp: "));
		Serial.println(currentTemperature, DEC);

		// HACK: Not sure why PIN_MODE_BUTTON is reading inverted, but it is.
		heating = !digitalRead(PIN_MODE_BUTTON) || (currentTemperature < targetTemperature);
#ifndef NO_HEATER
		if (heating) {
			ironHeater_setPulseWidth(MAX_HEATER_PULSE_WIDTH);
		} else {
			ironHeater_setPulseWidth(8);
		}
#endif // !NO_HEATER
	}

	display.clearDisplay();
	display.setCursor(0, 0);
	display.print(F("Target temp: "));
	display.print(int(targetTemperature));
	display.println(F("\370C"));
	display.print(F("Current temp: "));
	display.print(currentTemperature);
	display.println(F("\370C"));
	if (heating) {
		display.println(F("\30")); // up arrow
	} else {
		display.println(F("\31")); // down arrow
	}
	display.display();

#ifndef NO_HEATER
	ironHeater_update();
#endif // !NO_HEATER
}
