# Arduino Nano (or DCCduino Nano)
ARDUINO_FQBN:=arduino:avr:nano
ARDUINO_CHIP:=atmega328
ARDUINO_BOARD:=${ARDUINO_FQBN}:cpu=${ARDUINO_CHIP}
AVRDUDE_CHIP:=atmega328p
BOOTLOADER_IMG:=${HOME}/.arduino15/packages/arduino/hardware/avr/1.*/bootloaders/optiboot/optiboot_atmega328.hex
BAUD_RATE:=115200

# Actual SparkFun Pro Mini
#ARDUINO_FQBN:=arduino:avr:pro
#ARDUINO_CHIP:=atmega328
#ARDUINO_SPEED:=16MHz
#ARDUINO_BOARD:=${ARDUINO_FQBN}:cpu=${ARDUINO_SPEED}${ARDUINO_CHIP}
#AVRDUDE_CHIP:=atmega328p
#BOOTLOADER_IMG:=${HOME}/.arduino15/packages/SparkFun/hardware/avr/1.*/bootloaders/caterina/Caterina-promicro16.hex
#BAUD_RATE:=57600

# WAVGAT Pro Mini (clone)
#ARDUINO_FQBN:=WAV8F:AVR:lardu_328e
#ARDUINO_BOARD:=${ARDUINO_FQBN}
#AVRDUDE_CHIP:=atmega328p
#BOOTLOADER_IMG:=${HOME}/Arduino/hardware/WAV8F/AVR/bootloaders/WAV8fx8e/optiboot_lgt8f328d.hex
#BAUD_RATE:=57600

FTDI_DEV:=/dev/ttyUSB0
BUS_PIRATE_DEV:=/dev/buspirate


default: build/rt-soldering-station.ino.hex


build:
	mkdir $@

build/rt-soldering-station.ino.hex: rt-soldering-station.ino build
	arduino --verify --board ${ARDUINO_BOARD} --pref build.path=build $<


upload: build/rt-soldering-station.ino.hex /dev/ttyACM0
	stty -F /dev/ttyACM0 speed 1200
	stty -F /dev/ttyACM0 speed 57600
	avrdude -v -c avr109 -P /dev/ttyACM0 -p ${AVRDUDE_CHIP} -Uflash:w:$<:i

upload-usbtiny: build/rt-soldering-station.ino.hex
	avrdude -v -c usbtiny -p ${AVRDUDE_CHIP} -Uflash:w:$<:i

upload-buspirate: build/rt-soldering-station.ino.hex /dev/buspirate
	avrdude -v -c buspirate -P /dev/buspirate -p ${AVRDUDE_CHIP} -Uflash:w:$<:i

upload-ftdi: build/rt-soldering-station.ino.hex ${FTDI_DEV}
	#stty -F ${FTDI_DEV} speed 1200
	stty -F ${FTDI_DEV} speed ${BAUD_RATE}
	avrdude -v -c arduino -P ${FTDI_DEV} -p ${AVRDUDE_CHIP} -b${BAUD_RATE} -Uflash:w:$<:i


burn-bootloader-arduino: ${BOOTLOADER_IMG}
	avrdude -v -p ${AVRDUDE_CHIP} -c arduino -P /dev/ttyUSB0 -b19200 -Uflash:w:$<:i -Ulock:w:0x2F:m

burn-bootloader-usbtiny: ${BOOTLOADER_IMG}
	avrdude -v -p ${AVRDUDE_CHIP} -c usbtiny -Uflash:w:$<:i -Ulock:w:0x2F:m

burn-bootloader-buspirate: ${BOOTLOADER_IMG} ${BUS_PIRATE_DEV}
	avrdude -v -p ${AVRDUDE_CHIP} -c buspirate -P ${BUS_PIRATE_DEV} -Uflash:w:$<:i -Ulock:w:0x2F:m


/dev/ttyACM0:
	inotifywait -e create /dev
	inotifywait -e attrib /dev

/dev/ttyUSB0:
	inotifywait -e create /dev
	inotifywait -e attrib /dev

${BUS_PIRATE_DEV}:
	inotifywait -e create /dev
	inotifywait -e attrib /dev


clean:
	rm -f build/rt-soldering-station.ino.hex


.PHONY: default upload upload-usbtiny upload-buspirate burn-bootloader-usbtiny burn-bootloader-buspirate clean
