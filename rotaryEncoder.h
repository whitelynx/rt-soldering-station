#ifndef ROTARY_ENCODER_H
#define	ROTARY_ENCODER_H

/**
 * Look up table for value change based on encoder state.
 *
 * Look up using 5 bits: [lastDirection, lastA, lastB, currentA, currentB]
 * where lastDirection is 0 for negative and 1 for positive
 *
 * Encoder waveforms:
 *       ___     ___     ___
 * A ___|   |___|   |___|   |
 *     ___     ___     ___
 * B _|   |___|   |___|   |__
 *
 * A 0 0 1 1 0 0 1 1 0 0 1 1
 * B 0 1 1 0 0 1 1 0 0 1 1 0
 */
const char encoderLookup[32] = {
	0,  // 00 => 00 (last direction negative)
	1,  // 00 => 01 (last direction negative)
	-1, // 00 => 10 (last direction negative)
	-2, // 00 => 11 (last direction negative)
	-1, // 01 => 00 (last direction negative)
	0,  // 01 => 01 (last direction negative)
	-2, // 01 => 10 (last direction negative)
	1,  // 01 => 11 (last direction negative)
	1,  // 10 => 00 (last direction negative)
	-2, // 10 => 01 (last direction negative)
	0,  // 10 => 10 (last direction negative)
	-1, // 10 => 11 (last direction negative)
	-2, // 11 => 00 (last direction negative)
	-1, // 11 => 01 (last direction negative)
	1,  // 11 => 10 (last direction negative)
	0,  // 11 => 11 (last direction negative)

	0,  // 00 => 00 (last direction positive)
	1,  // 00 => 01 (last direction positive)
	-1, // 00 => 10 (last direction positive)
	2,  // 00 => 11 (last direction positive)
	-1, // 01 => 00 (last direction positive)
	0,  // 01 => 01 (last direction positive)
	2,  // 01 => 10 (last direction positive)
	1,  // 01 => 11 (last direction positive)
	1,  // 10 => 00 (last direction positive)
	2,  // 10 => 01 (last direction positive)
	0,  // 10 => 10 (last direction positive)
	-1, // 10 => 11 (last direction positive)
	2,  // 11 => 00 (last direction positive)
	-1, // 11 => 01 (last direction positive)
	1,  // 11 => 10 (last direction positive)
	0,  // 11 => 11 (last direction positive)
};


inline char rotaryEncoder_stateToDelta(unsigned char* lastDirection, unsigned char* lastState, unsigned char currentState) {
	char delta = encoderLookup[(*lastDirection) << 4 | (*lastState) << 2 | currentState];
	if (delta < 0) {
		*lastDirection = 0;
	} else if (delta > 0) {
		*lastDirection = 1;
	}
	*lastState = currentState;
	return delta;
}

#endif	/* ROTARY_ENCODER_H */
